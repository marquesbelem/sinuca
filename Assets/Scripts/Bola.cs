﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour
{
    public bool caiu;
    public string tipo_bola;

    void Update()
    {
        if (caiu)
          this.gameObject.SetActive(false);
    }


    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Buraco"))
        {
            caiu = true;
            tipo_bola = this.gameObject.tag;
        }
    }
}
