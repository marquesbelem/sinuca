﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador : MonoBehaviour
{

    LineRenderer linha;

    [SerializeField]
    BolaBranca bola_branca;
    [SerializeField]
    float forca = 10f;

    [SerializeField]
    float ritmo_jogo = 0.5f;

    GameObject[] bola_lisa;
    GameObject[] bola_listrada;

    bool vez_do_jogador = true;
    bool vez_da_AI;

    [SerializeField]
    string tipo_bola_jogador;

    [SerializeField]
    string tipo_bola_AI;
    GameObject bola_derrubar_AI;

    [SerializeField]
    bool continua_jogando;

    void Start()
    {
        linha = FindObjectOfType<LineRenderer>();
        bola_lisa = GameObject.FindGameObjectsWithTag("Bola Lisa");
        bola_listrada = GameObject.FindGameObjectsWithTag("Bola Listrada");
    }

    void Update()
    {
        if (vez_do_jogador)
            JogadaPlayer();

        if (vez_da_AI)
            JogadaAI();
    }

    void InicializaBolas()
    {
        bola_branca.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        for (int i = 0; i < bola_lisa.Length; i++)
            bola_lisa[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        for (int i = 0; i < bola_listrada.Length; i++)
            bola_listrada[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }

    void ParadaBola()
    {
        bola_branca.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bola_branca.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        for (int i = 0; i < bola_lisa.Length; i++)
        {
            if (!bola_lisa[i].GetComponent<Bola>().caiu)
            {
                bola_lisa[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
                bola_lisa[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
            else
            {
                if (string.IsNullOrEmpty(tipo_bola_jogador))
                    tipo_bola_jogador = "Bola Lisa";
            }

        }

        for (int i = 0; i < bola_listrada.Length; i++)
        {
            if (!bola_listrada[i].GetComponent<Bola>().caiu)
            {
                bola_listrada[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
                bola_listrada[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
            else
            {
                if (string.IsNullOrEmpty(tipo_bola_jogador))
                    tipo_bola_jogador = "Bola Listrada";
            }
        }

        bola_lisa = GameObject.FindGameObjectsWithTag("Bola Lisa");
        bola_listrada = GameObject.FindGameObjectsWithTag("Bola Listrada");
        
    }

    void ParadaBolasPlayer()
    {
        if (!linha.gameObject.activeSelf && bola_branca.GetComponent<Rigidbody>().velocity.magnitude < ritmo_jogo)
        {
            ParadaBola();

            if (!continua_jogando)
            {
                vez_do_jogador = false;
                vez_da_AI = true;
            }
        }
    }

    void ParadaBolasAI()
    {
        if (bola_branca.GetComponent<Rigidbody>().velocity.magnitude < ritmo_jogo)
        {
            ParadaBola();

            if (bola_branca.GetComponent<Rigidbody>().velocity.magnitude == 0 && continua_jogando)
            {
                Debug.Log("OLHA AQUI");
                linha.gameObject.SetActive(false);
            }

            if (!continua_jogando)
            {
                vez_da_AI = false;
                vez_do_jogador = true;
            }
        }
    }

    void JogadaPlayer()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 direcao = Vector3.zero;
        linha.enabled = true;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 bola_pos = new Vector3(bola_branca.transform.position.x, 0.1f, bola_branca.transform.position.z);
            Vector3 mouse_pos = new Vector3(hit.point.x, 0.1f, hit.point.z);

            linha.SetPosition(0, mouse_pos);
            linha.SetPosition(1, bola_pos);
            direcao = (mouse_pos - bola_pos).normalized;
        }


        if (Input.GetMouseButtonDown(0) && linha.gameObject.activeSelf)
        {
            InicializaBolas();
            linha.gameObject.SetActive(false);
            bola_branca.GetComponent<Rigidbody>().velocity = direcao * forca;
        }

        ParadaBolasPlayer();
    }

    void JogadaAI()
    {
        InicializaBolas();

        Vector3 direcao = Vector3.zero;
        Vector3 bola_pos = new Vector3(bola_branca.transform.position.x, 0.1f, bola_branca.transform.position.z);

        if (string.IsNullOrEmpty(tipo_bola_jogador))
        {
            tipo_bola_AI = "Bola Listrada";
        }
        else
        {
            if (tipo_bola_jogador == "Bola Lisa")
                tipo_bola_AI = "Bola Listrada";
            else if (tipo_bola_jogador == "Bola Listrada")
                tipo_bola_AI = "Bola Lisa";
        }

        bola_derrubar_AI = GameObject.FindGameObjectWithTag(tipo_bola_AI);
        direcao = (bola_derrubar_AI.transform.position - bola_pos).normalized;

        if (!linha.gameObject.activeSelf)
        {
            linha.enabled = false;
            bola_branca.GetComponent<Rigidbody>().velocity = direcao * forca;
            linha.gameObject.SetActive(true);
        }

        ParadaBolasAI();
    }

}

