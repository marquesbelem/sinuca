﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaBranca : MonoBehaviour
{
    Vector3 start_pos;
    bool reset;

    void Start()
    {
        start_pos = transform.position;
    }

    void Update()
    {
        if (transform.position.y < -0.1f)
        {
            transform.position = new Vector3(0, 0.08f, 0);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        if (reset)
        {
            reset = false;
            transform.position = start_pos;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    public void ResetBall()
    {
        reset = true;
    }

    void OnTriggerEnter(Collider coll) {

        if (coll.CompareTag("Buraco"))
            ResetBall();
    }
}
